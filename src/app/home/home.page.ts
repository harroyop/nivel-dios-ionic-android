import { Component } from '@angular/core';
import { isUndefined } from 'util';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

   // Variables:
   nombres: any;
   sueldo1: any;
   sueldo2: any;
   promedioSueldo: any;
   nivelSocioeco: any;
   sueldoSituacion: any;

   constructor() { }

   procesoDatos() {
     if (!isUndefined(this.sueldo1) && !isUndefined(this.sueldo2)) {
       if (!isNaN(Number(this.sueldo1)) && !isNaN(Number(this.sueldo2))) {

         // Muestro Promedio:
         this.promedioSueldo = (Number(this.sueldo1) + Number(this.sueldo2)) / 2;

         // Calculo NSE:
         if (this.promedioSueldo >= 26500) {
           this.nivelSocioeco = 'Nivel Socio Economico: A - ALTA SUPERIOR';
         } else if (this.promedioSueldo >= 12200 && this.promedioSueldo < 26500) {
           this.nivelSocioeco = 'Nivel Socio Economico: A1 - ALTO';
         } else if (this.promedioSueldo >= 6300 && this.promedioSueldo < 12200) {
           this.nivelSocioeco = 'Nivel Socio Economico: A2 - MEDIO ALTO';
         } else if (this.promedioSueldo >= 3500 && this.promedioSueldo < 6300) {
           this.nivelSocioeco = 'Nivel Socio Economico: B - MEDIO TIPICO';
         } else if (this.promedioSueldo >= 2900 && this.promedioSueldo < 3500) {
           this.nivelSocioeco = 'Nivel Socio Economico: B1 - MEDIO';
         } else if (this.promedioSueldo >= 2300 && this.promedioSueldo < 2900) {
           this.nivelSocioeco = 'Nivel Socio Economico: B2 - MEDIO BAJO';
         } else if (this.promedioSueldo >= 1500 && this.promedioSueldo < 2300) {
           this.nivelSocioeco = 'Nivel Socio Economico: C - BAJO SUPERIOR';
         } else if (this.promedioSueldo >= 1392 && this.promedioSueldo < 1500) {
           this.nivelSocioeco = 'Nivel Socio Economico: C1 - BAJO';
         } else if (this.promedioSueldo >= 1243 && this.promedioSueldo < 1392) {
           this.nivelSocioeco = 'Nivel Socio Economico: C2 - BAJO TIPICO';
         } else if (this.promedioSueldo >= 900 && this.promedioSueldo < 1243) {
           this.nivelSocioeco = 'Nivel Socio Economico: D - POBRE';
         } else if (this.promedioSueldo >= 660 && this.promedioSueldo < 900) {
           this.nivelSocioeco = 'Nivel Socio Economico: E - EXTREMA POBREZA';
         } else {
           this.nivelSocioeco = 'Sin Nivel Socio Economico';
         }

         // Calculo Situacion Sueldo:
         if (Number(this.sueldo2) > Number(this.sueldo1)) {
             this.sueldoSituacion = 'El sueldo del empleado mejoró.';
         } else if (Number(this.sueldo1) > Number(this.sueldo2)) {
             this.sueldoSituacion = 'El sueldo del empleado disminuyo.';
         } else {
             this.sueldoSituacion = 'El sueldo del empleado se mantiene constante.';
         }

       } else {
         this.promedioSueldo = '';
         this.nivelSocioeco = '';
         this.sueldoSituacion = '';
         alert('Los sueldos deben ser numericos.');
       }
     } else {
       this.promedioSueldo = '';
       this.nivelSocioeco = '';
       this.sueldoSituacion = '';
     }
   }

   alertBtn() {
     alert('Ya no soy necesario :(');
   }

}
